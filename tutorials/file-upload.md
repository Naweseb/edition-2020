# Uploading files to Gitlab

You can upload a file to Gitlab by using [git](https://git-scm.com/book/en/v2), the underlying version control system. However, if you are not familiar with the *git* protocol you can use the Web interface.

First, log into Gitlab and select the repository you want to add the file to. The screen capture below contains information such as the *branch* (master), the *repository* (edition-2020).
![screen capture](../images/sc-capture.png)
Pull the drop down button next to the plus button and select the *Upload file* menu item. Follow the instructions and click on the *Upload file* button in the interface. The new file will be added to the repository.
