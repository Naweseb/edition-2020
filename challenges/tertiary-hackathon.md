# Hackathon Competition for Tertiary Students

## Challenge 1 (weight: 70%)

The [Faculty of Computing and Informatics](http://fci.nust.na/) (FCI) at [NUST](http://www.nust.na) has embarked upon a software development project to manage the __postgraduate programme__ process, including *application*, *proposal*, and *thesis* and  *examination* management. The FCI Postgraduate Management System Application will improve the processing of the whole cycle of application for the thesis, the proposal of a topic, the thesis and examination of the students. The diagram below captures the main stages of the process.
![screen capture](../images/postgraduate-process.png)

### Requirements

We further elicit the requirements as follows. The different users of the System are the *students*, *supervisors*, *Head of Department* (HOD), *Faculty Internal Examiner* (FIE) and *Higher Degree Committee* (HDC). Any External Supervisor (or External Examiner) is not included in the System, because their regular communication will solely be with the NUST supervisor (or the HoD). The *Dean* and HOD will have an overview of all the information concerning students in the whole process.

At the application stage, a student starts the process by completing and submitting an application form, which is stored in the system. Thereafter, a supervisor based on his/her profile (skills, specialism, interests, workload) can browse the applications and express his/her interests. The HOD then compiles the augmented list (applications and supervisor interests). The applicants whose application received interest will be invited for an interview. Subsequent deliberations among panel members will decide on the acceptance or not of each application and the outcome communicated to the candidate.

The first milestone for a student who recently enrolled in a postgraduate programme is to successfully present his/her proposal (within the prescribed period). Once the draft of the proposal is ready and approved by the supervisor, the student will upload the proposal to the system. The HOD can then view the proposal and assign FIEs for its evaluation. A proposal that has been sanctioned by all FIEs is ready for submission to HDC. The faculty HDC representative can view the proposal or upload the resolution from HDC after evaluation by the committee. When HDC approves a proposal, the HOD will change the student status with the mention *final admission*. For proposals that have not been approved by HDC, the submission to HDC sub-process will be repeated until approval.

A student whose proposal has been approved enters the *thesis* stage. While the actual research is being conducted, the student will register at the beginning of each semester and submit regular reports attesting to her progress.

When the (research) work is completed and the draft of the thesis ready (with the approval of the supervisor), the student will upload the thesis to the system. The HOD will then send the thesis to the *appointed* external examiner(s) and await their assessment. When the examiner assessment is received, he/she will upload it into the system. When the thesis handling is completed, the results are compiled and submitted to HDC for its endorsement. At the end of the process, a successful student's status will change to *graduate*. For cases with minor changes, they must be effected to the satisfaction of an appointed member of the Faculty. Finally, for cases that require an extended amount of the work, the necessary steps should be taken and the thesis resubmitted.

### Forms

An application form should include the following information:
* Student Number
* Full Names
* Student Country/Citizenship
* Skills Set
* Interest
* Motivation
* Qualification and Specialisations


A potential supervisor profile includes the following information:
* Staff Number
* Full Names
* Skills Set
* Interest
* Workload

## Challenge 2 (weight: 30%)

### Requirements

Consider a point calculation system for grade 12 learners in Namibia. Your task is to create a system that takes in a learner’s names and the six subjects written plus the symbol obtained, then calculate the total points obtained. The symbols can either be 1-4 or U for High level and A*-G or U for ordinary level with respective points given in the table below, it is up to your system to figure this out. However, remember that in case the learner failed English, then the learner gets 0 points and a fail.


|Ordinary Level Symbol| Mark|Higher Level Symbol|Mark|
|:--|:--|:--|:--|
|A+|8|1|10|
|A|7|2|9|
|B|6|3|8|
|C|5|4|7|
|D|4|U|0|
|E|3|||
|F|2|||
|G|1|||
|U|0|||

Additional requirements are as follows:
1. Only valid symbols are accepted;
2. After calculation the system should indicate whether the learner is eligible for tertiary provided they have 25 points and at least an E in English;
3. Symbols can be entered in either case (lower or upper case);
4. If a student skipped the exam for a given subject, then a – (dash) is entered.

Furthermore, the system should ask the learners their preferred two fields of study at tertiary and then indicate whether they would be admitted into that field or not. If not admitted the system should provide a reason. Below are all the fields of study at NUST and their admission requirements.

| Field of study/Faculty| Required Points| Additional Requirements|
|---------------------- | -------------- | ---------------------- |
| Management Sciences | 26 points in 5 subjects|D Symbol in English|
|||E symbol in Mathematics|
|Human Sciences| 26 points in 5 subjects| C Symbol in English|
|Computing and Informatics|30 points in 5 subjects| C Symbol in Mathematics|
| Engineering| 37 points in 5 subjects| Mathematics L3|
|||level 4 for English and Physical Science|
| Health and Applied Sciences | 30 points in 5 subjects|C Symbol in English|
|||D symbol in Biology, Mathematics and Physical Science|
| Natural Resources and Spatial Sciences | 30 points in 5 subjects|C Symbol in English|
|||D symbol in Geography and Mathematics|

### Example

Consider a student with the following information:

* Full name: Joshua Maponga
* gender: Male
* Subjects and symbols:
   * English: 2
   * Khoekhoegowab: 1
   * Biology: B
   * Chemistry: C
   * Mathematics: B
   * Agriculture: A
* Field of study 1: Engineering Sciences
* Field of study 2: Medicine

The response from your system could be formatted as follows:

Dear Mr. Joshua Maponga, you have 38 points in 5 subjects. Congratulations! you are eligible for tertiary studies in Namibia.

Field of study 1: Unfortunately, you have not been admitted into Engineering Sciences.
	Comment: Physical Science missing on list of subjects

Field of study 2: Medicine is not offered at NUST
